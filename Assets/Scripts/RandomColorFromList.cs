﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColorFromList : MonoBehaviour
{
    public Color[] colors;
    void Start()
    {
        gameObject.GetComponent<SpriteRenderer>().color = colors[Random.Range(0, colors.Length)];
    }
}
